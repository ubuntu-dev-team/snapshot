# Brazilian Portuguese translation for Snapshot.
# Copyright (C) 2023 Snapshot's COPYRIGHT HOLDER
# This file is distributed under the same license as the Snapshot package.
# Leônidas Araújo <leorusvellt@hotmail.com>, 2023.
# Gabriel Mattoso <gabriel.reis.mattoso@gmail.com>, 2023.
# Rafael Fontenelle <rafaelff@gnome.org>, 2023.
# Alex Jr <alexjrsh@proton.me>, 2023.
#
msgid ""
msgstr ""
"Project-Id-Version: Snapshot main\n"
"Report-Msgid-Bugs-To: https://gitlab.gnome.org/GNOME/snapshot/-/issues\n"
"POT-Creation-Date: 2024-02-23 00:23+0000\n"
"PO-Revision-Date: 2024-03-08 11:16-0300\n"
"Last-Translator: Leônidas Araújo <leorusvellt@hotmail.com>\n"
"Language-Team: Brazilian Portuguese <https://br.gnome.org/traducao>\n"
"Language: pt_BR\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n > 1);\n"
"X-Generator: Poedit 3.3.1\n"

#: data/org.gnome.Snapshot.desktop.in.in:3
#: data/org.gnome.Snapshot.metainfo.xml.in.in:7 data/resources/ui/window.ui:6
#: data/resources/ui/window.ui:13 src/main.rs:27 src/widgets/window.rs:266
msgid "Camera"
msgstr "Câmera"

# Dúvido com relação ao modo verbal
#: data/org.gnome.Snapshot.desktop.in.in:4
#: data/org.gnome.Snapshot.metainfo.xml.in.in:8
msgid "Take pictures and videos"
msgstr "Tire fotos e vídeos"

#. Translators: Search terms to find this application. Do NOT translate or localize the semicolons! The list MUST also end with a semicolon!
#: data/org.gnome.Snapshot.desktop.in.in:10
msgid "picture;photos;camera;webcam;snapshot;"
msgstr ""
"picture;photos;camera;webcam;snapshot;imagem;fotos;camera;webcam;captura;"
"imagens;"

#: data/org.gnome.Snapshot.gschema.xml.in:16
#: data/org.gnome.Snapshot.gschema.xml.in:17
msgid "Default window width"
msgstr "Largura padrão da janela"

#: data/org.gnome.Snapshot.gschema.xml.in:21
#: data/org.gnome.Snapshot.gschema.xml.in:22
msgid "Default window height"
msgstr "Altura padrão da janela"

#: data/org.gnome.Snapshot.gschema.xml.in:26
msgid "Default window maximized behaviour"
msgstr "Comportamento padrão da janela maximizada"

#: data/org.gnome.Snapshot.gschema.xml.in:31
msgid "Play shutter sound"
msgstr "Reproduzir som do obturador"

#: data/org.gnome.Snapshot.gschema.xml.in:32
msgid "Whether to play a shutter sound when taking pictures"
msgstr "Se deve reproduzir um som do obturador ao tirar fotos"

#: data/org.gnome.Snapshot.gschema.xml.in:36
msgid "Show composition guidelines"
msgstr "Mostrar linhas de composição"

#: data/org.gnome.Snapshot.gschema.xml.in:37
msgid "Whether to show composition guidelines when using the camera"
msgstr "Se as linhas de composição devem ser mostradas ao usar a câmera"

#: data/org.gnome.Snapshot.gschema.xml.in:41
msgid "Countdown timer"
msgstr "Temporizador de contagem regressiva"

#: data/org.gnome.Snapshot.gschema.xml.in:42
msgid "The duration of the countdown for taking photos, in seconds"
msgstr "A duração da contagem regressiva para tirar fotos, em segundos"

#: data/org.gnome.Snapshot.gschema.xml.in:46
msgid "Capture mode"
msgstr "Modo captura"

#: data/org.gnome.Snapshot.gschema.xml.in:47
msgid "Whether to take pictures or videos"
msgstr "Para tirar fotos ou vídeos (Fuzzy)"

#: data/org.gnome.Snapshot.gschema.xml.in:51
msgid "Picture format"
msgstr "Formato da foto"

#: data/org.gnome.Snapshot.gschema.xml.in:52
msgid "Whether to save pictures as png or jpeg"
msgstr "Se deseja salvar imagens como png ou jpeg"

#: data/org.gnome.Snapshot.gschema.xml.in:56
msgid "Video format"
msgstr "Formato do vídeo"

#: data/org.gnome.Snapshot.gschema.xml.in:57
msgid "Whether to save videos as mp4 or webm"
msgstr "Se deseja salvar vídeos como mp4 ou webm"

#: data/org.gnome.Snapshot.metainfo.xml.in.in:10
msgid "Take pictures and videos on your computer, tablet, or phone."
msgstr "Tire fotos e vídeos em seu computador, tablet ou telefone."

#: data/org.gnome.Snapshot.metainfo.xml.in.in:17
msgid "Camera's main interface"
msgstr "Interface principal do Câmera"

#: data/org.gnome.Snapshot.metainfo.xml.in.in:21
msgid "Camera's gallery viewer"
msgstr "Visualizador de galeria do Câmera"

#: data/org.gnome.Snapshot.metainfo.xml.in.in:119 src/widgets/window.rs:272
msgid "The GNOME Project"
msgstr "O Projeto GNOME"

#: data/resources/ui/camera.ui:97 data/resources/ui/camera.ui:170
#: data/resources/ui/camera.ui:341 data/resources/ui/camera.ui:476
msgid "Main Menu"
msgstr "Menu principal"

#: data/resources/ui/camera.ui:107
msgid "No Camera Found"
msgstr "Nenhuma câmera encontrada"

#: data/resources/ui/camera.ui:108
msgid "Connect a camera device"
msgstr "Conectar um dispositivo de câmera"

#: data/resources/ui/camera.ui:196 data/resources/ui/camera.ui:294
#: data/resources/ui/camera.ui:407 data/resources/ui/camera.ui:498
msgid "Picture Mode"
msgstr "Modo foto"

#: data/resources/ui/camera.ui:215 data/resources/ui/camera.ui:313
#: data/resources/ui/camera.ui:426 data/resources/ui/camera.ui:517
msgid "Recording Mode"
msgstr "Modo gravação"

#: data/resources/ui/camera.ui:237 data/resources/ui/camera.ui:271
#: data/resources/ui/camera.ui:374 data/resources/ui/camera.ui:546
#: data/resources/ui/camera.ui:577
msgid "Countdown"
msgstr "Contagem regressiva"

#. TRANSLATORS this indicates the countdown is disabled
#: data/resources/ui/camera.ui:580
msgid "None"
msgstr "Nenhuma"

#. TRANSLATORS this is time for a countdown in seconds
#: data/resources/ui/camera.ui:585
msgid "3s"
msgstr "3s"

#. TRANSLATORS this is time for a countdown in seconds
#: data/resources/ui/camera.ui:590
msgid "5s"
msgstr "5s"

#. TRANSLATORS this is time for a countdown in seconds
#: data/resources/ui/camera.ui:595
msgid "10s"
msgstr "10s"

#: data/resources/ui/camera.ui:603
msgid "_Preferences"
msgstr "_Preferências"

#: data/resources/ui/camera.ui:607
msgid "_Keyboard Shortcuts"
msgstr "_Atalhos de teclado"

#: data/resources/ui/camera.ui:611
msgid "_About Camera"
msgstr "_Sobre o Câmera"

#: data/resources/ui/camera_controls.ui:17
msgid "Open Gallery"
msgstr "Abrir galeria"

#. Set a fallback tooltip
#: data/resources/ui/camera_controls.ui:25 src/widgets/shutter_button.rs:70
#: src/widgets/shutter_button.rs:119
msgid "Take Picture"
msgstr "Tirar foto"

#: data/resources/ui/camera_controls.ui:35
msgid "Select Camera"
msgstr "Selecionar câmera"

#: data/resources/ui/camera_controls.ui:48
msgctxt "Switch between front and back camera button"
msgid "Switch Camera"
msgstr "Trocar de câmera"

#: data/resources/ui/gallery.ui:46
msgid "Gallery Menu"
msgstr "Menu galeria"

#: data/resources/ui/gallery.ui:94
msgid "Previous Image"
msgstr "Imagem anterior"

#: data/resources/ui/gallery.ui:107
msgid "Next Image"
msgstr "Próxima imagem"

#: data/resources/ui/gallery.ui:150 src/widgets/gallery.rs:115
msgid "Open in Image Viewer"
msgstr "Abrir no visualizador de imagens"

#: data/resources/ui/preferences_window.ui:10
msgid "General"
msgstr "Geral"

#: data/resources/ui/preferences_window.ui:16
msgid "_Shutter Sound"
msgstr "_Som do obturador"

#: data/resources/ui/preferences_window.ui:23
msgid "_Composition Guidelines"
msgstr "_Linhas de composição"

#: data/resources/ui/shortcuts.ui:11
msgctxt "shortcut window"
msgid "General"
msgstr "Geral"

#: data/resources/ui/shortcuts.ui:14
msgctxt "shortcut window"
msgid "Show Shortcuts"
msgstr "Mostra atalhos"

#: data/resources/ui/shortcuts.ui:20
msgctxt "shortcut window"
msgid "Main Menu"
msgstr "Menu principal"

#: data/resources/ui/shortcuts.ui:26
msgctxt "Shortcut window description"
msgid "Open Preferences"
msgstr "Abre as preferências"

#: data/resources/ui/shortcuts.ui:32
msgctxt "shortcut window"
msgid "Quit"
msgstr "Sai"

#: data/resources/ui/shortcuts.ui:40
msgctxt "shortcut window"
msgid "Camera"
msgstr "Câmera"

#: data/resources/ui/shortcuts.ui:43
msgctxt "shortcut window"
msgid "Picture Mode"
msgstr "Modo foto"

#: data/resources/ui/shortcuts.ui:49
msgctxt "shortcut window"
msgid "Recording Mode"
msgstr "Modo gravação"

#: data/resources/ui/shortcuts.ui:55
msgctxt "shortcut window"
msgid "Take Picture or Start Recording"
msgstr "Tira foto ou começa gravação"

#: data/resources/ui/shortcuts.ui:61
msgctxt "shortcut window"
msgid "Show/Hide Composition Guidelines"
msgstr "Mostrar/ocultar linhas de composição"

#: data/resources/ui/shortcuts.ui:69
msgctxt "shortcut window"
msgid "Gallery"
msgstr "Galeria"

#: data/resources/ui/shortcuts.ui:72
msgctxt "shortcut window"
msgid "Open/Close Gallery"
msgstr "Abre/fecha galeria"

#: data/resources/ui/shortcuts.ui:78
msgctxt "shortcut window"
msgid "Next"
msgstr "Próximo"

#: data/resources/ui/shortcuts.ui:84
msgctxt "shortcut window"
msgid "Previous"
msgstr "Anterior"

#: data/resources/ui/window.ui:21
msgid "Gallery"
msgstr "Galeria"

#. TRANSLATORS This is the image format presented in the preferences
#. window.
#: src/enums.rs:27
msgid "JPEG"
msgstr "JPEG"

#. TRANSLATORS Do NOT translate {date}. This will appear as, e.g. "Photo
#. from 2023-05-21 11-05-59.12345" and it will be used as a file name.
#: src/utils.rs:23
msgid "Photo from {date}"
msgstr "Foto de {date}"

#. TRANSLATORS Do NOT translate {number}. This will appear as, e.g.
#. "Photo 12345" and it will be used as a file name.
#: src/utils.rs:28
msgid "Photo {number}"
msgstr "Foto {number}"

#. TRANSLATORS Do NOT translate {date}. This will appear as, e.g.
#. "Recording from 2023-05-21 11-05-59.12345" and it will be used as a
#. file name.
#: src/utils.rs:41
msgid "Recording from {date}"
msgstr "Gravação de {date}"

#. TRANSLATORS Do NOT translate {number}. This will appear as, e.g.
#. "Recording 12345" and it will be used as a file name.
#: src/utils.rs:46
msgid "Recording {number}"
msgstr "Gravação {number}"

#: src/utils.rs:128
msgid "_Copy Picture"
msgstr "_Copiar foto"

#: src/utils.rs:130
msgid "_Copy Video"
msgstr "_Copiar vídeo"

#: src/utils.rs:132
msgid "_Delete"
msgstr "_Excluir"

#: src/widgets/camera.rs:429
msgid "Could not play camera stream"
msgstr "Não foi possível reproduzir stream da câmera"

#: src/widgets/gallery.rs:117
msgid "Open in Video Player"
msgstr "Abrir no reprodutor de vídeos"

#: src/widgets/gallery.rs:386
msgid "Copied to clipboard"
msgstr "Copiado para a área de transferência"

#: src/widgets/gallery.rs:434
msgid "Picture deleted"
msgstr "Foto apagada"

#: src/widgets/gallery.rs:436
msgid "Video deleted"
msgstr "Vídeo excluído"

#: src/widgets/shutter_button.rs:81
msgid "Start Recording"
msgstr "Começar gravação"

#: src/widgets/shutter_button.rs:92
msgid "Stop Recording"
msgstr "Parar gravação"

#: src/widgets/window.rs:74 src/widgets/window.rs:315
msgid "Could not take picture"
msgstr "Não foi possível tirar foto"

#: src/widgets/window.rs:78 src/widgets/window.rs:319
msgid "Could not record video"
msgstr "Não foi possível gravar vídeo"

#: src/widgets/window.rs:271
msgid "translator-credits"
msgstr ""
"Leônidas Araújo <leorusvellt@hotmail.com>, 2023\n"
"Rafael Fontenelle <rafaelff@gnome.org>, 2023"

#~ msgid "Last Camera Used ID"
#~ msgstr "ID da última câmera utilizada"

#~ msgid "An ID representing the last used camera"
#~ msgstr "Um ID que representa a última câmera utilizada"

#~ msgid "Snapshot"
#~ msgstr "Snapshot"

#~ msgid "_About Snapshot"
#~ msgstr "_Sobre o Snapshot"

#~ msgid "Back"
#~ msgstr "Volta"

#~ msgid "Photo"
#~ msgstr "Foto"

#~ msgid "Recording"
#~ msgstr "Gravação"

#~ msgid "Maximiliano Sandoval"
#~ msgstr "Maximiliano Sandoval"
